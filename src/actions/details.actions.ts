import Expression from '../models/expression.model';

export const FETCH_DETAILS_STARTED  = 'FETCH_DETAILS_STARTED';
export const FETCH_DETAILS_DONE     = 'FETCH_DETAILS_DONE';
export const FETCH_DETAILS_FAILED   = 'FETCH_DETAILS_FAILED';

export const fetchDetailsStarted = () => ({
    type: FETCH_DETAILS_STARTED
});

export const fetchDetailsDone = (expression: Expression) => ({
    type: FETCH_DETAILS_DONE,
    expression
});

export const fetchDetailsFailed = (errorText: string) => ({
    type: FETCH_DETAILS_FAILED,
    errorText
});