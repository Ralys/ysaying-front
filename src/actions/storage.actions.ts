import ExpressionLight from '../models/expressionLight.model';

export const FETCH_STORAGE_STARTED  = 'FETCH_STORAGE_STARTED';
export const FETCH_STORAGE_DONE     = 'FETCH_STORAGE_DONE';
export const FETCH_STORAGE_FAILED   = 'FETCH_STORAGE_FAILED';

export const fetchStorageStarted = () => ({
    type: FETCH_STORAGE_STARTED
});

export const fetchStorageDone = (expressions: ExpressionLight[]) => ({
    type: FETCH_STORAGE_DONE,
    expressions
});

export const fetchStorageFailed = (errorText: string) => ({
    type: FETCH_STORAGE_FAILED,
    errorText
});