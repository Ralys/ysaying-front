import ExpressionPageResponse from '../models/expressionPageResponse.model';

export const FETCH_EXPRESSIONS_STARTED  = 'FETCH_EXPRESSIONS_STARTED';
export const FETCH_EXPRESSIONS_DONE     = 'FETCH_EXPRESSIONS_DONE';
export const FETCH_EXPRESSIONS_FAILED   = 'FETCH_EXPRESSIONS_FAILED';
export const UPDATE_SEARCH_TEXT = 'UPDATE_SEARCH_TEXT';

export const fetchExpressionsStarted = (isReady: boolean) => ({
    type: FETCH_EXPRESSIONS_STARTED,
    isReady
});

export const fetchExpressionsDone = (expressionPageResponse: ExpressionPageResponse) => ({
    type: FETCH_EXPRESSIONS_DONE,
    expressionPageResponse
});

export const fetchExpressionsFailed = (errorText: string) => ({
    type: FETCH_EXPRESSIONS_FAILED,
    errorText
});

export const updateSearchText = (searchText: string) => ({
    type: UPDATE_SEARCH_TEXT,
    searchText
});