import ExpressionPageResponse from '../models/expressionPageResponse.model';

export const FETCH_CENTURIES_STARTED  = 'FETCH_CENTURIES_STARTED';
export const FETCH_CENTURIES_DONE     = 'FETCH_CENTURIES_DONE';
export const FETCH_CENTURIES_FAILED   = 'FETCH_CENTURIES_FAILED';

export const FETCH_CENTURY_EXPRESSIONS_STARTED   = 'FETCH_CENTURY_EXPRESSIONS_STARTED';
export const FETCH_CENTURY_EXPRESSIONS_DONE   = 'FETCH_CENTURY_EXPRESSIONS_DONE';
export const FETCH_CENTURY_EXPRESSIONS_FAILED   = 'FETCH_CENTURY_EXPRESSIONS_FAILED';

export const fetchCenturiesStarted = () => ({
    type: FETCH_CENTURIES_STARTED
});

export const fetchCenturiesDone = (centuries: number[]) => ({
    type: FETCH_CENTURIES_DONE,
    centuries
});

export const fetchCenturiesFailed = (errorText: string) => ({
    type: FETCH_CENTURIES_FAILED,
    errorText
});

export const fetchCenturyExpressionsStarted = (selectedCentury: number, isReady: boolean) => ({
    type: FETCH_CENTURY_EXPRESSIONS_STARTED,
    selectedCentury,
    isReady
});

export const fetchCenturyExpressionsDone = (expressionPageResponse: ExpressionPageResponse) => ({
    type: FETCH_CENTURY_EXPRESSIONS_DONE,
    expressionPageResponse
});

export const fetchCenturyExpressionsFailed = (errorText: string) => ({
    type: FETCH_CENTURY_EXPRESSIONS_FAILED,
    errorText
});