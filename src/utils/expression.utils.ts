import ExpressionLight from '../models/expressionLight.model';

export const markAsSeen = (expressions: ExpressionLight[], alreadySeenExpressions: ExpressionLight[]) => {
    return expressions.map((expression: ExpressionLight) => {
        const match = alreadySeenExpressions
            .find((seen: ExpressionLight) => seen.id === expression.id);
        
        if (match) {
            expression.seenDate = match.seenDate;
        }

        return expression;
    });
}