import { POINT_CONVERSION_COMPRESSED } from 'constants';
import Axios from 'axios';
import { BACKEND_URL } from '../models/constants';

export interface CenturyService {

    findAll(): Promise<number[]>;

}

class CenturyServiceImpl implements CenturyService {

    centuries: number[] = [];

    findAll(): Promise<number[]> {
        if (this.centuries.length > 0) {
            return Promise.resolve(this.centuries);
        }

        return Axios.get(`${BACKEND_URL}/centuries`)
            .then(({ data: response }) => 
                response.map((century: any) => Number(century))
            );
    }

}

export default new CenturyServiceImpl();