import ExpressionPageResponse from '../models/expressionPageResponse.model';
import Expression from '../models/expression.model';
import ExpressionLight from '../models/expressionLight.model';
import Axios from 'axios';
import { BACKEND_URL } from '../models/constants';

export interface ExpressionService {

    findAll(first: number, limit: number): Promise<ExpressionPageResponse>;
    findByTitle(title: string, first: number, limit: number): Promise<ExpressionPageResponse>;
    findByCentury(century: number, first: number, limit: number): Promise<ExpressionPageResponse>;
    findById(id: string): Promise<Expression>;
    findRandom(): Promise<ExpressionLight>;

}

let counter = 0;

class ExpressionServiceImpl implements ExpressionService {

    findAll(first: number, limit: number): Promise<ExpressionPageResponse> {
        return Axios.get(`${BACKEND_URL}/expressions?first=${first}&limit=${limit}`)
            .then(({ data: response }) => 
                new ExpressionPageResponse(response.first, response.total, response.data)
            );
    }

    findByTitle(title: string, first: number, limit: number): Promise<ExpressionPageResponse> {
        return Axios.get(`${BACKEND_URL}/expressions?title=${title}&first=${first}&limit=${limit}`)
            .then(({ data: response }) => 
                new ExpressionPageResponse(response.first, response.total, response.data)
            );
    }

    findByCentury(century: number, first: number, limit: number): Promise<ExpressionPageResponse> {
        return Axios.get(`${BACKEND_URL}/expressions?century=${century}&first=${first}&limit=${limit}`)
            .then(({ data: response }) => 
                new ExpressionPageResponse(response.first, response.total, response.data)
            );
    }
    
    findById(id: string): Promise<Expression> {
        return Axios.get(`${BACKEND_URL}/expressions/${id}`)
            .then(({ data: response }) => 
                new Expression(response.id, response.title, response.content, response.century, response.image)
            );
    }

    findRandom(): Promise<ExpressionLight> {
        return Axios.get(`${BACKEND_URL}/randomExpression`)
            .then(({ data: response }) => 
                new ExpressionLight(response.id, response.title, response.century)
            );
    }

}

export default new ExpressionServiceImpl();