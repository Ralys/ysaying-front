import Expo from 'expo';

import ExpressionLight from '../models/expressionLight.model';

export interface StorageService {

    getExpressions(): Promise<ExpressionLight[]>;
    addExpression(expression: ExpressionLight): Promise<ExpressionLight[]>;
    reset(): Promise<void>;

}

class StorageServiceImpl implements StorageService {

    static TAG = 'EXPRESSIONS';

    expressions: ExpressionLight[] = [];

    private initExpressions(): Promise<ExpressionLight[]> {
        return Expo.SecureStore.setItemAsync(StorageServiceImpl.TAG, JSON.stringify(this.expressions))
            .then(() => this.expressions);
    }

    async getExpressions(): Promise<ExpressionLight[]> {
        if (this.expressions.length > 0) {
            return this.expressions;
        }

        try {
            const expressionsText = await Expo.SecureStore.getItemAsync(StorageServiceImpl.TAG)
            const expressions = expressionsText && JSON.parse(expressionsText);
            const expressionsFromStorage = expressions.map((expression: any) => 
                new ExpressionLight(
                    expression.id, 
                    expression.title, 
                    expression.century, 
                    expression.seenDate
                ));

            this.expressions = expressionsFromStorage;
            return this.expressions;
        } catch (e) {
            return this.initExpressions();
        }
    }

    addExpression(expression: ExpressionLight): Promise<ExpressionLight[]> {
        const match = this.expressions.find((exp) => exp.id === expression.id);
        
        if (match) {
            return Promise.resolve(this.expressions);
        } else {
            expression.seenDate = new Date().toISOString();
            this.expressions.push(expression);

            return Expo.SecureStore.setItemAsync(StorageServiceImpl.TAG, JSON.stringify(this.expressions))
                .then(() => this.expressions);
        }
    }

    reset(): Promise<void> {
        return Expo.SecureStore.deleteItemAsync(StorageServiceImpl.TAG);
    }

}

export default new StorageServiceImpl();