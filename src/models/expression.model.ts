export default class Expression {

    id: string;
    title: string;
    content: string;
    century?: number;
    image?: string;

    constructor(id: string, title: string, content: string, century?: number, image?: string) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.century = century;
        this.image = image || 'http://ralys.fr/e-cv/assets/img/backgrounds/bg-header.jpg';
    }

}