export default class SortDefinition {
    
    static ASC = 'ASC';
    static DESC = 'DESC';

    static ORDERS = [ SortDefinition.ASC, SortDefinition.DESC ];

    property: string;
    order: string;

    constructor(property: string, order: string) {
        if (!SortDefinition.ORDERS.includes(order)) {
            throw new Error(`Must be either ${SortDefinition.ORDERS.join(' or ')}`);
        }

        this.property = property;
        this.order = order;
    }
}