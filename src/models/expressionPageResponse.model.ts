import ExpressionLight from './expressionLight.model';

export default class ExpressionPageResponse {

    first: number;
    total: number;
    data: ExpressionLight[];

    constructor(first: number = 0, total: number = 0, data: ExpressionLight[] = []) {
        this.first = first;
        this.total = total;
        this.data = data;
    }

}