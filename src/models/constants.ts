export const BLUE = '#0098EC';
export const DARKBLUE = '#00689D';
export const GRAY = '#F0EFF4';
export const STATUS_BAR_BLUE = '#0095F8';
// export const BACKEND_URL = 'http://192.168.1.13:8080/ysaying-api';
export const BACKEND_URL = 'https://ysaying-api.herokuapp.com/ysaying-api';

export const ROUTE_KEYS = {
    SEARCH: 'Recherche',
    CONSULTED: 'Consultés',
    CENTURY: 'Century',
    EXPRESSION_DETAILS: 'ExpressionDetails'
};