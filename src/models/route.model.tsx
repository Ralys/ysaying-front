import { NavigationScreenProp, NavigationState } from 'react-navigation';

type NavigationConsumer = (navigation: NavigationScreenProp<NavigationState>) => void;

export default class Route {

    name: string;
    handler: NavigationConsumer;

    constructor(name: string, handler: NavigationConsumer) {
        this.name = name;
        this.handler = handler;
    }
    
}