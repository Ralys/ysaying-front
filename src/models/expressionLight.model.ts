export default class ExpressionLight {

    id: string;
    title: string;
    century?: number;
    seenDate?: string;

    constructor(id: string, title: string, century?: number, seenDate?: string) {
        this.id = id;
        this.title = title;
        this.century = century;
        this.seenDate = seenDate;
    }

}