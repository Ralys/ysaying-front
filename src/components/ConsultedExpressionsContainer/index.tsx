import { connect } from 'react-redux';

import storageService from '../../services/storage.service';
import ConsultedExpressionsContainer from './ConsultedExpressionsContainer';

import { fetchStorageDone, fetchStorageFailed } from '../../actions/storage.actions';

const mapStateToProps = (state: any) => ({
    isReady: state.storage.isReady,
    expressions: state.storage.expressions
});

const mapDispatchToProps = (dispatch: Function) => ({
    async getStoredExpressions() {
        try {
            const storedExpressions = await storageService.getExpressions();
            dispatch(fetchStorageDone(storedExpressions));
        } catch (e) {
            dispatch(fetchStorageFailed(e.message));
        }
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(ConsultedExpressionsContainer);