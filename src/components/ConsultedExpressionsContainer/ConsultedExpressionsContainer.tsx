import React from 'react';
import { Container, Content, Spinner } from 'native-base';

import ExpressionList from '../ExpressionList/ExpressionList';
import SortDefinition from '../../models/sortDefinition.model';
import { BLUE } from '../../models/constants';

export default class ConsultedExpressionsContainer extends React.Component<any> {

    componentWillMount() {
        this.props.getStoredExpressions();
    }

    render() {
        const { isReady, expressions } = this.props;
        const { navigate } = this.props.navigation;

        return (
            <Container>
                <Content>{ 
                    isReady ? <ExpressionList
                        sort= { new SortDefinition('seenDate', SortDefinition.DESC) }
                        navigate={navigate} 
                        expressions={expressions}
                        shouldShowMore={false} 
                    /> :
                    <Spinner color={BLUE} />
                }</Content>
          </Container>
        );
    }

}