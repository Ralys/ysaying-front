import React from 'react';
import { Header, Left, Icon, Body, Text, Button } from 'native-base';
import { HeaderProps, NavigationActions } from 'react-navigation';
import styles from './styles';
import { BackHandler, NativeEventSubscription } from 'react-native';

export default class CustomHeader extends React.Component<HeaderProps> {

    backButtonSubscription?: NativeEventSubscription;

    componentWillMount() {
        this.backButtonSubscription = BackHandler.addEventListener('hardwareBackPress', () => {
            requestAnimationFrame(() => 
                this.props.navigation.dispatch(NavigationActions.back())
            );

            return true;
        });
    }

    componentWillUnmount() {
        this.backButtonSubscription && this.backButtonSubscription.remove();
    }

    goBack = () => {
        const { scene, navigation } = this.props;
        navigation.goBack(scene.descriptor.key);
    };

    render() {
        const { scene } = this.props;

        return (
            <Header style={styles.header}>
                <Left>
                    <Button transparent rounded block onPress={this.goBack}>
                        <Icon style={styles.icon} name="arrow-back" />
                    </Button>
                </Left>
                <Body style={styles.body}>
                    <Text style={styles.text}>{scene.descriptor.options.title}</Text>
                </Body>
            </Header>
        );
    }

}