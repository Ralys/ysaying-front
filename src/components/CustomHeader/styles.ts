import { StyleSheet } from 'react-native';
import { BLUE } from '../../models/constants';

export default StyleSheet.create({
    header: { 
        backgroundColor: BLUE
    },
    icon: {
        color: 'white'
    },
    body: {
        flexGrow: 2
    },
    text: {
        color: 'white'
    }
});
