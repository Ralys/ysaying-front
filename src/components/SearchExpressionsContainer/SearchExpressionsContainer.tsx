import React from 'react';
import { 
    Container, Header, Item, Icon, Input, Content, Spinner
} from 'native-base';

import ExpressionList from '../ExpressionList/ExpressionList';
import { BLUE } from '../../models/constants';
import { markAsSeen } from '../../utils/expression.utils';
import styles from './styles';
import ExpressionPageResponse from '../../models/expressionPageResponse.model';

export default class SearchExpressionsContainer extends React.Component<any> {

    componentWillMount() {
        const { 
            expressionPageResponse, 
            searchText, 
            getStoredExpressions, 
            loadExpressions 
        } = this.props;
        
        getStoredExpressions();
        loadExpressions(expressionPageResponse, searchText);
    }

    getExpressions = () => {
        const { expressionPageResponse, alreadySeenExpressions } = this.props;
        return markAsSeen(expressionPageResponse.data, alreadySeenExpressions)
    };

    handleMore = () => {
        const { expressionPageResponse, loadExpressions, searchText } = this.props;
        return loadExpressions(expressionPageResponse, searchText);
    };

    shouldShowMore = () => {
        const { expressionPageResponse } = this.props;
        return expressionPageResponse.total - expressionPageResponse.data.length > 10;
    };

    handleSearch = (searchText: string) => {
        if (searchText.length === 0 || searchText.length > 1) {
            const { updateSearchText, loadExpressions } = this.props;

            setTimeout(() => {
                updateSearchText(searchText);
                loadExpressions(new ExpressionPageResponse(), searchText);
            }, 300);
        }
    };

    render() {
        const { isReady } = this.props;
        const { navigate } = this.props.navigation;

        return (
            <Container>
                <Header searchBar rounded style={styles.header}>
                    <Item>
                        <Icon name="ios-search" />
                        <Input 
                            placeholder="Expression, dicton ..." 
                            onChangeText={this.handleSearch}
                        />
                    </Item>
                </Header>
                <Content>{ 
                    isReady ? <ExpressionList 
                        navigate={navigate} 
                        expressions={this.getExpressions()}
                        shouldShowMore={this.shouldShowMore()}
                        onMore={this.handleMore}
                    /> :
                    <Spinner color={BLUE} />
                }</Content>
          </Container>
        );
    }

}