import { connect } from 'react-redux';

import expressionService from '../../services/expression.service';
import storageService from '../../services/storage.service';
import SearchExpressionsContainer from './SearchExpressionsContainer';
import { 
    fetchExpressionsStarted, 
    fetchExpressionsDone, 
    fetchExpressionsFailed,
    updateSearchText 
} from '../../actions/search.actions';
import { fetchStorageDone, fetchStorageFailed } from '../../actions/storage.actions';
import ExpressionPageResponse from '../../models/expressionPageResponse.model';

const mapStateToProps = (state: any) => ({
    isReady: state.search.isReady,
    searchText: state.search.searchText,
    alreadySeenExpressions: state.storage.expressions,
    expressionPageResponse: state.search.expressionPageResponse
});

const mapDispatchToProps = (dispatch: Function) => ({
    async getStoredExpressions() {
        try {
            const storedExpressions = await storageService.getExpressions();
            dispatch(fetchStorageDone(storedExpressions));
        } catch (e) {
            dispatch(fetchStorageFailed(e.message));
        }
    },

    updateSearchText(searchText: string) {
        dispatch(updateSearchText(searchText));
    },

    async loadExpressions(currentPage: ExpressionPageResponse, searchText: string) {
        dispatch(fetchExpressionsStarted(currentPage.data.length > 0));

        try {
            const receivedPage = searchText.length > 0 ?
                await expressionService.findByTitle(searchText, currentPage.data.length, 10) : 
                await expressionService.findAll(currentPage.data.length, 10);
            
            const expressionPageResponse = new ExpressionPageResponse(
                receivedPage.first,
                receivedPage.total,
                [ ...currentPage.data, ...receivedPage.data ]
            );

            dispatch(fetchExpressionsDone(expressionPageResponse));
        } catch (e) {
            dispatch(fetchExpressionsFailed(e.message));
        }
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchExpressionsContainer);