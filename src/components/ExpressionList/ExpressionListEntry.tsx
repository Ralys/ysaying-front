import React from 'react';
import { 
    Text, Icon, ListItem, Body, Right
} from 'native-base';

import ExpressionLight from '../../models/expressionLight.model';
import styles from './styles';
import CenturyButton from '../CenturyButton/CenturyButton';
import { ROUTE_KEYS } from '../../models/constants';

interface ExpressionListEntryProps {
    navigate: Function;
    expression: ExpressionLight;
}

export default class ExpressionListEntry extends React.Component<ExpressionListEntryProps> {

    showDetails = () => {
        const { navigate, expression } = this.props;
        navigate(ROUTE_KEYS.EXPRESSION_DETAILS, { expression });
    };

    render() {
        const { navigate, expression } = this.props;

        return (
            <ListItem onPress={ this.showDetails }>
                <Body style={styles.body}>{ 
                    expression.century && <CenturyButton 
                        navigate={navigate} 
                        century={expression.century}
                    />
                }</Body>
                <Body>
                    <Text>{expression.title}</Text>
                </Body>
                <Right>
                    { expression.seenDate && <Icon active name="eye" /> }
                </Right>
                <Right>
                    <Icon active name="ios-arrow-forward" />
                </Right>
            </ListItem>
        );
    }

}