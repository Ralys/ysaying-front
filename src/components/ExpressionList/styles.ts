import { StyleSheet } from 'react-native';
import { BLUE } from '../../models/constants';

export default StyleSheet.create({
    list: {
        backgroundColor: 'white',
        marginTop: 10
    },
    body: {
        flexGrow: 0.4
    },
    button: { 
        margin: 10, 
        backgroundColor: BLUE, 
        alignSelf: 'center' 
    },
    text: {
        color: 'white'
    }
});