import React from 'react';
import { List, Button, Text, Container, Content, Spinner } from 'native-base';

import ExpressionLight from '../../models/expressionLight.model';
import ExpressionListEntry from './ExpressionListEntry';
import styles from './styles';
import SortDefinition from '../../models/sortDefinition.model';
import { BLUE, STATUS_BAR_BLUE } from '../../models/constants';
import { StatusBar } from 'react-native';

interface ExpressionListProps {
    navigate: Function;
    sort?: SortDefinition;
    expressions: ExpressionLight[];
    onMore?: () => Promise<any>;
    shouldShowMore: boolean;
}

interface ExpressionListState {
    isLoadingMore: boolean;
}

export default class ExpressionList extends React.Component<ExpressionListProps, ExpressionListState> {
    
    static defaultProps = {
        sort: new SortDefinition('title', SortDefinition.ASC)
    };

    state = {
        isLoadingMore: false
    };

    componentWillMount() {
        StatusBar.setBackgroundColor(STATUS_BAR_BLUE);
    }

    applySort = (expressions: ExpressionLight[]) : ExpressionLight[] => {
        const { sort } = this.props;

        if (!sort) {
            return expressions;
        }

        return expressions.sort((first: any, second: any) => {
            const propertyOfFirst = first[sort.property];
            const propertyOfSecond = second[sort.property];

            if (propertyOfFirst < propertyOfSecond) {
                return SortDefinition.ASC === sort.order ? -1: 1;
            } else if (propertyOfFirst > propertyOfSecond) {
                return SortDefinition.ASC === sort.order ? 1 : -1;
            } else {
                return 0;
            }
        });
    }

    loadMore = () => {
        const { onMore } = this.props;
        if (!onMore) return;

        this.setState({ isLoadingMore: true });
        
        onMore()
            .then(() => this.setState({ isLoadingMore: false }))
            .catch(() => this.setState({ isLoadingMore: false }));
    };

    render() {
        const { navigate, expressions, shouldShowMore } = this.props;
        const { isLoadingMore } = this.state;

        return (
            <Content>
                <List style={styles.list}>{
                    this.applySort(expressions).map((expression, index) => (
                        <ExpressionListEntry key={index} navigate={navigate} expression={expression} />
                    ))
                }</List>
                {
                    shouldShowMore && (
                        isLoadingMore ? <Spinner color={BLUE} /> :
                        <Button style={styles.button} onPress={() => this.loadMore()}>
                            <Text style={styles.text}>More...</Text>
                        </Button>
                    )
                }
            </Content>
        );
    }

}