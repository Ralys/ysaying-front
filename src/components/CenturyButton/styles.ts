import { StyleSheet } from 'react-native';

import { BLUE } from '../../models/constants';

export default StyleSheet.create({
    button: {
        height: 'auto',
        backgroundColor: BLUE,
        borderRadius: 8
    },
    text: {
      color: 'white',
      fontSize: 10
    }
});