import React from 'react';
import { Button, Text } from 'native-base';
import { ROUTE_KEYS } from '../../models/constants';
import styles from './styles';

interface CenturyButtonProps {
    navigate: Function;
    century: number;
}

export default class CenturyButton extends React.Component<CenturyButtonProps> {
    
    showCentury = () => {
        const { navigate, century } = this.props;
        navigate(ROUTE_KEYS.CENTURY, { century });
    };

    render() {
        const { century } = this.props;

        return (
            <Button style={styles.button} onPress={this.showCentury}>
                <Text style={styles.text}>{ `${century}ème` }</Text>
            </Button>
        );
    }

}