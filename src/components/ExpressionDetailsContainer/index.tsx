import { connect } from 'react-redux';

import expressionService from '../../services/expression.service';
import storageService from '../../services/storage.service';
import ExpressionDetailsContainer from './ExpressionDetailsContainer';
import ExpressionLight from '../../models/expressionLight.model';

import { fetchDetailsStarted, fetchDetailsDone, fetchDetailsFailed } from '../../actions/details.actions';
import { fetchStorageDone, fetchStorageFailed } from '../../actions/storage.actions';

const mapStateToProps = (state: any) => ({
    isReady: state.details.isReady,
    expression: state.details.expression
});

const mapDispatchToProps = (dispatch: Function) => ({
    async findExpressionById(id: string) {
        dispatch(fetchDetailsStarted());

        try {
            const expression = await expressionService.findById(id);
            dispatch(fetchDetailsDone(expression));
        } catch (e) {
            dispatch(fetchDetailsFailed(e.message));
        }
    },

    async storeExpression(expression: ExpressionLight) {
        try {
            const storedExpressions = await storageService.addExpression(expression);
            dispatch(fetchStorageDone(storedExpressions));
        } catch (e) {
            dispatch(fetchStorageFailed(e.message));
        }
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(ExpressionDetailsContainer);