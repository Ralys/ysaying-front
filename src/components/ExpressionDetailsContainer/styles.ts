import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    imageBackground: { 
        width: '100%', 
        height: 120, 
        justifyContent: 'center' 
    },
    imageBackgroundView: {
        flexDirection:'row', 
        flexWrap: 'wrap', 
        justifyContent: 'center'
    },
    titleText: {
        textAlign: 'center',
        fontSize: 20,
        color: 'white',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        borderRadius: 8,
        padding: 4
    },
    centuryView: {
        margin: 10, 
        alignSelf: 'center'
    },
    contentText: { 
        backgroundColor: 'white', 
        color: '#16212c', 
        textAlign: 'justify', 
        fontSize: 16,
        lineHeight: 25, 
        padding: 5
    }
});