import React from 'react';
import { Text, View, Container } from 'native-base';
import { ImageBackground, ScrollView } from 'react-native';

import Expression from '../../models/expression.model';
import CenturyButton from '../CenturyButton/CenturyButton';
import styles from './styles';

interface ExpressionDetailsProps {
    navigate: Function;
    expression: Expression;
}

export default class ExpressionDetails extends React.Component<ExpressionDetailsProps> {

    render() {
        const { navigate } = this.props;
        const { title, content, century, image } = this.props.expression;

        return (
            <ScrollView>
                <ImageBackground
                    source={{ uri: image }}
                    style={styles.imageBackground}
                > 
                    <View style={styles.imageBackgroundView}>
                        <Text style={styles.titleText}>
                            {title}
                        </Text>
                    </View>
                </ImageBackground>

                <View style={styles.centuryView}>{ 
                    century && <CenturyButton 
                        navigate={navigate} 
                        century={century}
                    />
                }</View>

                <Text style={styles.contentText}>
                    {content}
                </Text>
            </ScrollView>
        );
    }

}