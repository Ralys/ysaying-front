import React from 'react';
import { Container, Content, Spinner } from 'native-base';

import ExpressionDetails from './ExpressionDetails';
import { BLUE } from '../../models/constants';

export default class ExpressionDetailsContainer extends React.Component<any> {

    componentWillMount() {
        const { id } = this.props.navigation.state.params.expression;
        this.props.findExpressionById(id);
    }

    componentDidMount() {
        const { expression } = this.props.navigation.state.params;
        this.props.storeExpression(expression);
    }

    render() {
        const { navigation, isReady, expression  } = this.props;

        return (
            <Container>
                <Content>{
                    isReady ? (
                        expression && <ExpressionDetails navigate={navigation.navigate} expression={expression} />
                    )   : (
                        <Spinner color={BLUE} />
                    )
                }</Content>
            </Container>
        );
    }

}