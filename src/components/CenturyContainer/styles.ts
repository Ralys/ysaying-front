import { StyleSheet } from 'react-native';

import { BLUE, GRAY, DARKBLUE } from '../../models/constants';

export default StyleSheet.create({
    header: { 
        backgroundColor: GRAY,
        height: 200
    },
    grid: {
        marginTop: 20
    },
    row: { 
        paddingBottom: 2, 
        justifyContent: 'center'
    },
    button: { 
        margin: 2,
        padding: 0,
        height: 'auto',
        borderRadius: 0,
        backgroundColor: BLUE
    },
    activeButton: {
        backgroundColor: DARKBLUE
    },
    text: { 
        fontSize: 10, 
        color: 'white' 
    }
});