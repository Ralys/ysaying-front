import React from 'react';
import { Container, Header, Content, Spinner, Text, Button, Grid, Row } from 'native-base';

import ExpressionList from '../ExpressionList/ExpressionList';
import { markAsSeen } from '../../utils/expression.utils';
import { BLUE } from '../../models/constants';
import { StyleSheet } from 'react-native';
import styles from './styles';
import ExpressionPageResponse from '../../models/expressionPageResponse.model';

export default class CenturyContainer extends React.Component<any> {

    async componentWillMount() {
        const { expressionPageResponse } = this.props;
        const { century } = this.props.navigation.state.params;
        const { loadCenturies, loadExpressionsForCentury } = this.props;

        await loadCenturies();
        await loadExpressionsForCentury(expressionPageResponse, century);
    }

    getExpressions = () => {
        const { expressionPageResponse, alreadySeenExpressions } = this.props;
        return markAsSeen(expressionPageResponse.data, alreadySeenExpressions)
    };

    groupBy = (centuries: number[], nbElementsPerGroup: number) => {
        const grouped: number[][] = [ [] ];
        
        centuries.forEach((century) => {
            const group = grouped.find(arr => arr.length < nbElementsPerGroup);
            
            if (group) {
                group.push(century);
            } else {
                const newGroup = [ century ];
                grouped.push(newGroup);
            }
        });

        return grouped;
    };

    changeCentury = (century: number) => {
        const { navigation, loadExpressionsForCentury } = this.props;

        navigation.setParams({ century });
        loadExpressionsForCentury(new ExpressionPageResponse(), century);
    };

    handleMore = () => {
        const { expressionPageResponse, loadExpressionsForCentury, selectedCentury } = this.props;
        return loadExpressionsForCentury(expressionPageResponse, selectedCentury);
    };

    shouldShowMore = () => {
        const { expressionPageResponse } = this.props;
        return expressionPageResponse.total - expressionPageResponse.data.length > 10;
    };

    formatNumber = (value: number) => {
        return (value < 10) ? '0' + value : value;
    };
    
    render() {
        const { isReady, centuries, selectedCentury } = this.props;
        const { navigate } = this.props.navigation;

        return (
            <Container>
                <Header style={styles.header}>
                    <Grid style={styles.grid}>{
                        this.groupBy(centuries, 3).map((group, indexGroup) =>
                            <Row key={indexGroup} style={styles.row}>{
                                group.map((century: number, index: number) =>
                                    <Button 
                                        key={index} 
                                        onPress={() => this.changeCentury(century)}
                                        style={StyleSheet.flatten([
                                            styles.button, 
                                            selectedCentury === century && styles.activeButton
                                        ])}
                                    >
                                        <Text style={styles.text}>{ `${this.formatNumber(century)}ème` }</Text>
                                    </Button>
                                )
                            }</Row>
                        )
                    }</Grid>
                </Header>
                <Content>{ 
                    isReady ? <ExpressionList 
                        navigate={navigate} 
                        expressions={this.getExpressions()} 
                        shouldShowMore={this.shouldShowMore()}
                        onMore={this.handleMore}
                    /> :
                    <Spinner color={BLUE} />
                }</Content>
          </Container>
        );
    }

}