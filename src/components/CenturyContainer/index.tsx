import { connect } from 'react-redux';

import expressionService from '../../services/expression.service';
import CenturyContainer from './CenturyContainer';
import centuryService from '../../services/century.service';

import { 
    fetchCenturiesStarted, fetchCenturiesDone, fetchCenturiesFailed, 
    fetchCenturyExpressionsStarted, fetchCenturyExpressionsDone, 
    fetchCenturyExpressionsFailed 
} from '../../actions/century.actions';
import ExpressionPageResponse from '../../models/expressionPageResponse.model';

const mapStateToProps = (state: any) => ({
    isReady: state.century.isReady,
    centuries: state.century.centuries,
    selectedCentury: state.century.selectedCentury,
    alreadySeenExpressions: state.storage.expressions,
    expressionPageResponse: state.century.expressionPageResponse
});

const mapDispatchToProps = (dispatch: Function) => ({
    async loadCenturies() {
        dispatch(fetchCenturiesStarted());

        try {
            const centuries = await centuryService.findAll();
            dispatch(fetchCenturiesDone(centuries));
        } catch (e) {
            dispatch(fetchCenturiesFailed(e.message));
        }
    },

    async loadExpressionsForCentury(currentPage: ExpressionPageResponse, century: number) {
        dispatch(fetchCenturyExpressionsStarted(century, currentPage.data.length > 0));

        try {
            const receivedPage = await expressionService.findByCentury(century, currentPage.data.length, 10);
            const expressionPageResponse = new ExpressionPageResponse(
                receivedPage.first,
                receivedPage.total,
                [ ...currentPage.data, ...receivedPage.data ]
            );

            dispatch(fetchCenturyExpressionsDone(expressionPageResponse));
        } catch (e) {
            dispatch(fetchCenturyExpressionsFailed(e.message));
        }
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(CenturyContainer);