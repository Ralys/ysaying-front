import { createMaterialTopTabNavigator } from 'react-navigation';

import SearchExpressionsContainer from '../SearchExpressionsContainer';
import ConsultedExpressionsContainer from '../ConsultedExpressionsContainer';
import { ROUTE_KEYS } from '../../models/constants';

const Home = createMaterialTopTabNavigator({
    [ROUTE_KEYS.SEARCH]: SearchExpressionsContainer,
    [ROUTE_KEYS.CONSULTED]: ConsultedExpressionsContainer
});

export default Home;