import { connect } from 'react-redux';

import {
    reduxifyNavigator,
    createReactNavigationReduxMiddleware,
} from 'react-navigation-redux-helpers';

import Main from './Main';

const middleware = createReactNavigationReduxMiddleware(
    'root',
    (state: any) => state.nav
);

const MainWithNavigationState = reduxifyNavigator(Main, 'root');

const mapStateToProps = (state: any) => ({
    state: state.nav,
});

const MainNavigator = connect(mapStateToProps)(MainWithNavigationState);

export { Main, MainNavigator, middleware };