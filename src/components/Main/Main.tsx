import React from 'react';
import { createDrawerNavigator, createStackNavigator, HeaderProps } from 'react-navigation';

import Sidebar from './Sidebar';
import Home from '../Home/Home';
import ExpressionDetailsContainer from '../ExpressionDetailsContainer';
import CenturyContainer from '../CenturyContainer';
import CustomHeader from '../CustomHeader/CustomHeader';
import { ROUTE_KEYS } from '../../models/constants';

const PagesWithMenu = createDrawerNavigator({
    Home
}, {
    contentComponent: (props) => <Sidebar {...props} />
});

PagesWithMenu.navigationOptions = { header: null };

const Main = createStackNavigator({
    PagesWithMenu,
    [ROUTE_KEYS.EXPRESSION_DETAILS]: {
        screen: ExpressionDetailsContainer,
        navigationOptions: (props: any) => ({
            title: props.navigation.state.params.expression.title
        })
    },
    [ROUTE_KEYS.CENTURY]: {
        screen: CenturyContainer,
        navigationOptions: (props: any) => ({
            title: `Expressions du ${props.navigation.state.params.century}ème siècle`
        })
    }
}, {
    navigationOptions: {
        header: (props: HeaderProps) => <CustomHeader {...props} />
    }
});

export default Main;