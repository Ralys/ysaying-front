import Route from '../../models/route.model';
import expressionService from '../../services/expression.service';
import { ROUTE_KEYS } from '../../models/constants';
import storageService from '../../services/storage.service';

const routes = [ 
    new Route('Recherche', ({ navigate }) => navigate(ROUTE_KEYS.SEARCH)),
    new Route('Consultés', ({ navigate }) => navigate(ROUTE_KEYS.CONSULTED)),
    new Route('Expression aléatoire', async ({ navigate }) => {
        const expression = await expressionService.findRandom();
        navigate(ROUTE_KEYS.EXPRESSION_DETAILS, { expression });
    }),
    new Route('Réinitialiser les consultés', async ({ navigate }) => {
        await storageService.reset();
    })
];

export default routes;