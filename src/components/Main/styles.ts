import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    image: {
        height: 120,
        alignSelf: 'stretch',
        justifyContent: 'center',
        alignItems: 'center',
        resizeMode: 'cover'
    }
});

export const image = {
    source: require('../../../assets/drawer-cover.png')
}