import React from 'react';
import { Image } from 'react-native';
import { Container, Content, Text, List, ListItem } from 'native-base';
import { NavigationScreenProp, NavigationState } from 'react-navigation';

import routes from './routes';
import Route from '../../models/route.model';
import styles, { image } from './styles';

interface SideBarProps {
    navigation: NavigationScreenProp<NavigationState>
}

export default class SideBar extends React.Component<SideBarProps> {

    redirect = (route: Route) => {
        const { navigation } = this.props;
        route.handler(navigation);
    };

    render() {
        return (
            <Container>
                <Content>
                    <Image
                        source={image.source}
                        style={styles.image} 
                    />
                    <List
                        dataArray={routes}
                        renderRow={(route: Route) => (
                            <ListItem
                                button
                                onPress={() => this.redirect(route)}>
                                <Text>{route.name}</Text>
                            </ListItem>
                        )}
                    />
                </Content>
            </Container>
        );
    }
}