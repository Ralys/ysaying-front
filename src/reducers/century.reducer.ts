import ExpressionPageResponse from '../models/expressionPageResponse.model';

import { 
    FETCH_CENTURIES_STARTED, FETCH_CENTURIES_DONE, 
    FETCH_CENTURIES_FAILED, FETCH_CENTURY_EXPRESSIONS_STARTED, 
    FETCH_CENTURY_EXPRESSIONS_DONE, FETCH_CENTURY_EXPRESSIONS_FAILED
} from '../actions/century.actions';

const initialState = {
    isReady: false,
    centuries: [],
    selectedCentury: null,
    expressionPageResponse: new ExpressionPageResponse()
};

export default (prevState = initialState, action: any)  => {

    switch (action.type) {
        case FETCH_CENTURIES_STARTED:
            return { ...prevState, isReady: false };

        case FETCH_CENTURIES_DONE:
            return { ...prevState, centuries: [ ...action.centuries ] };

        case FETCH_CENTURIES_FAILED:
            return { ...prevState, isReady: true } ;

        case FETCH_CENTURY_EXPRESSIONS_STARTED:
            return { ...prevState, isReady: action.isReady, selectedCentury: action.selectedCentury };

        case FETCH_CENTURY_EXPRESSIONS_DONE:
            return { ...prevState, isReady: true, expressionPageResponse: action.expressionPageResponse };

        case FETCH_CENTURY_EXPRESSIONS_FAILED:
            return { ...prevState, isReady: true };

        default:
            return { ...prevState };
    }

};