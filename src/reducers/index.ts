import { combineReducers } from 'redux';
import { createNavigationReducer } from 'react-navigation-redux-helpers';

import { Main } from '../components/Main';
import searchReducer from './search.reducer';
import storageReducer from './storage.reducer';
import detailsReducer from './details.reducer';
import centuryReducer from './century.reducer';

const navReducer = createNavigationReducer(Main);

export default combineReducers({
    nav: navReducer,
    search: searchReducer,
    storage: storageReducer,
    details: detailsReducer,
    century: centuryReducer
});