import { FETCH_DETAILS_STARTED, FETCH_DETAILS_DONE, FETCH_DETAILS_FAILED } from '../actions/details.actions';

const initialState = {
    isReady: false,
    expression: null
};

export default (prevState = initialState, action: any)  => {

    switch (action.type) {
        case FETCH_DETAILS_STARTED:
            return { ...prevState, isReady: false };

        case FETCH_DETAILS_DONE:
            return { ...prevState, isReady: true, expression: action.expression };

        case FETCH_DETAILS_FAILED:
            return { ...prevState, isReady: true};

        default:
            return { ...prevState  };
    }

};