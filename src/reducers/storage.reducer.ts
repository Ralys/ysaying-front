import { 
    FETCH_STORAGE_STARTED, FETCH_STORAGE_DONE, FETCH_STORAGE_FAILED
} from '../actions/storage.actions';

const initialState = {
    isReady: false,
    expressions: []
};

export default (prevState = initialState, action: any)  => {

    switch (action.type) {
        case FETCH_STORAGE_STARTED:
            return { ...prevState, isReady: false };

        case FETCH_STORAGE_DONE:
            return { ...prevState, isReady: true, expressions: [ ...action.expressions ] };

        case FETCH_STORAGE_FAILED:
            return { ...prevState, isReady: true } ;

        default:
            return { ...prevState };
    }

};