import ExpressionPageResponse from '../models/expressionPageResponse.model';

import { 
    FETCH_EXPRESSIONS_STARTED, 
    FETCH_EXPRESSIONS_DONE, 
    FETCH_EXPRESSIONS_FAILED, 
    UPDATE_SEARCH_TEXT
} from '../actions/search.actions';

const initialState = {
    isReady: false,
    searchText: '',
    alreadySeenExpressions: [],
    expressionPageResponse: new ExpressionPageResponse()
};

export default (prevState = initialState, action: any)  => {

    switch (action.type) {
        case FETCH_EXPRESSIONS_STARTED:
            return { ...prevState, isReady: action.isReady };

        case FETCH_EXPRESSIONS_DONE:
            return { ...prevState, isReady: true, expressionPageResponse: action.expressionPageResponse };

        case FETCH_EXPRESSIONS_FAILED:
            return { ...prevState, isReady: true };

        case UPDATE_SEARCH_TEXT:
            return { ...prevState, searchText: action.searchText };
            
        default:
            return { ...prevState };
    }

};