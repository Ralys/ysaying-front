import React from 'react';
import Expo, { AppLoading } from 'expo';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { Container } from 'native-base';

import reducers from './src/reducers';
import { MainNavigator, middleware } from './src/components/Main';

const store = createStore(reducers, applyMiddleware(middleware));

export default class App extends React.Component<{}> {
  
  state = {
    isReady: false
  };

  async loadFonts() {
    return Expo.Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
      'Ionicons': require('@expo/vector-icons/fonts/Ionicons.ttf')
    });
  }

  handleFinish = () => {
    this.setState({ isReady: true });
  }

  render() {
    const { isReady } = this.state;

    return (
      <Container>
        <Provider store={store}>{
          isReady ? 
            <MainNavigator /> : 
            <AppLoading
              startAsync={this.loadFonts}
              onFinish={this.handleFinish}
              onError={console.warn}
            />
        }</Provider>
      </Container>
    );
  }
}
